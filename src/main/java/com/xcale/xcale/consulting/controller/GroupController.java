package com.xcale.xcale.consulting.controller;

import com.xcale.xcale.consulting.domain.Group;
import com.xcale.xcale.consulting.dto.MessageResponseDTO;
import com.xcale.xcale.consulting.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/group")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @GetMapping()
    public ResponseEntity getAllGroup(){
        return new ResponseEntity(groupService.findAll(), HttpStatus.OK);
    }

    @GetMapping("{phoneNumberAdmin}")
    public ResponseEntity getAllGroupByPhoneNumber(@PathVariable String phoneNumber){
        MessageResponseDTO messageDTO = groupService.getAllGroupByPhoneNumber(phoneNumber);
        return new ResponseEntity((List<Group>)messageDTO.getMsg(), messageDTO.getHttpStatus());
    }

    //@DeleteMapping("/{idGroup}/{phoneNumber}")


    @PostMapping()
    public ResponseEntity createGroup(@RequestBody Group group){
        MessageResponseDTO messageDTO = groupService.createGroup(group);
        return new ResponseEntity((String)messageDTO.getMsg(), messageDTO.getHttpStatus());
    }
}
