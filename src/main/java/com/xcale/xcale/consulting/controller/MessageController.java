package com.xcale.xcale.consulting.controller;

import com.xcale.xcale.consulting.dto.MessageDTO;
import com.xcale.xcale.consulting.dto.MessageResponseDTO;
import com.xcale.xcale.consulting.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PostMapping
    public ResponseEntity createMessage(@RequestBody MessageDTO messageDTO){
        MessageResponseDTO messageResponseDTO = messageService.createMessage(messageDTO);
        return new ResponseEntity(messageResponseDTO.getMsg(), messageResponseDTO.getHttpStatus());
    }
}
