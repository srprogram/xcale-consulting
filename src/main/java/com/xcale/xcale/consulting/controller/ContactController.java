package com.xcale.xcale.consulting.controller;

import com.xcale.xcale.consulting.domain.Contact;
import com.xcale.xcale.consulting.dto.MessageResponseDTO;
import com.xcale.xcale.consulting.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contact")
public class ContactController {

    @Autowired
    private ContactService contactService;

    @GetMapping
    public ResponseEntity getAllContact(){
        return new ResponseEntity(contactService.getAllContact(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createContact(@RequestBody Contact contact){
        MessageResponseDTO messageDTO = contactService.createContact(contact);
        return new ResponseEntity(messageDTO.getMsg(), messageDTO.getHttpStatus());
    }

}
