package com.xcale.xcale.consulting.repository;

import com.xcale.xcale.consulting.domain.Contact;
import com.xcale.xcale.consulting.domain.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {

    List<Group> findByCreatedBy(Contact contact);
}
