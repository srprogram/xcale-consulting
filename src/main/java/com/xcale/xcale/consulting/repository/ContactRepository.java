package com.xcale.xcale.consulting.repository;

import com.xcale.xcale.consulting.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {

    Contact findByNumber(String phoneNumber);
}
