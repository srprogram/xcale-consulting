package com.xcale.xcale.consulting.service;

import com.xcale.xcale.consulting.domain.Contact;
import com.xcale.xcale.consulting.dto.MessageResponseDTO;
import com.xcale.xcale.consulting.exception.ContactBasicInformationException;
import com.xcale.xcale.consulting.repository.ContactRepository;
import com.xcale.xcale.consulting.exception.DuplicatedNumberException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ContactService {

    private MessageResponseDTO messageDTO;

    private ContactRepository contactRepository;

    @Autowired
    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public MessageResponseDTO getAllContact(){
        return new MessageResponseDTO(contactRepository.findAll(), HttpStatus.OK);
    }

    public MessageResponseDTO createContact(Contact pcontact) {
        messageDTO = new MessageResponseDTO();
        try {
            if(pcontact.getName() == null || pcontact.getName().trim().isEmpty() ||
                pcontact.getNumber() == null || pcontact.getNumber().trim().isEmpty()){
                throw new ContactBasicInformationException();
            }

            Contact contact = contactRepository.findByNumber(pcontact.getNumber());
            if(contact != null)
                throw new DuplicatedNumberException();

            contactRepository.save(pcontact);
            messageDTO.setMsg("The contact was created successfully");
            messageDTO.setHttpStatus(HttpStatus.CREATED);

        }catch (ContactBasicInformationException e){
            messageDTO.setMsg("All user must have phone number and a name.");
            messageDTO.setHttpStatus(HttpStatus.BAD_REQUEST);
        }catch (DuplicatedNumberException e){
            messageDTO.setMsg("Exist a user with the number you are trying to use.");
            messageDTO.setHttpStatus(HttpStatus.BAD_REQUEST);
        }
        return messageDTO;
    }
}
