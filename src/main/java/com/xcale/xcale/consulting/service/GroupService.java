package com.xcale.xcale.consulting.service;

import com.xcale.xcale.consulting.domain.Contact;
import com.xcale.xcale.consulting.domain.Group;
import com.xcale.xcale.consulting.dto.MessageResponseDTO;
import com.xcale.xcale.consulting.exception.GroupBasicInformationException;
import com.xcale.xcale.consulting.exception.InexistentUserException;
import com.xcale.xcale.consulting.repository.ContactRepository;
import com.xcale.xcale.consulting.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GroupService {

    private GroupRepository groupRepository;

    private ContactRepository contactRepository;

    private MessageResponseDTO messageDTO;

    @Autowired
    public GroupService(GroupRepository groupRepository, ContactRepository contactRepository) {
        this.groupRepository = groupRepository;
        this.contactRepository = contactRepository;
    }

    public MessageResponseDTO createGroup(Group group){
        messageDTO = new MessageResponseDTO();
        try{
            if( (group.getName() == null || group.getName().trim().isEmpty()) ||
                    group.getCreatedBy() == null ){
                throw new GroupBasicInformationException();
            }

            Optional<Contact> contact = contactRepository.findById(group.getCreatedBy().getId());
            if (!contact.isPresent())
                throw new InexistentUserException();

            groupRepository.save(group);
            messageDTO.setMsg("Group created successfully.");
            messageDTO.setHttpStatus(HttpStatus.CREATED);

        }catch (GroupBasicInformationException e){
            messageDTO.setMsg("Verify the information provided.");
            messageDTO.setHttpStatus(HttpStatus.BAD_REQUEST);
        }catch (InexistentUserException e){
            messageDTO.setMsg("The group must have an existent user.");
            messageDTO.setHttpStatus(HttpStatus.BAD_REQUEST);
        }
        return messageDTO;
    }

    public MessageResponseDTO findAll() {
        return new MessageResponseDTO(groupRepository.findAll(),HttpStatus.OK);
    }

    public MessageResponseDTO getAllGroupByPhoneNumber(String phoneNumber) {
        messageDTO = new MessageResponseDTO();
        try{
            Contact contact = contactRepository.findByNumber(phoneNumber);
            if (contact == null)
                throw new InexistentUserException();

            messageDTO.setMsg(groupRepository.findByCreatedBy(contact));
            messageDTO.setHttpStatus(HttpStatus.OK);

        }catch (InexistentUserException e){
            messageDTO.setMsg("The group must have an existent user.");
            messageDTO.setHttpStatus(HttpStatus.BAD_REQUEST);
        }
        return messageDTO;
    }
}
