package com.xcale.xcale.consulting.service;

import com.xcale.xcale.consulting.domain.Contact;
import com.xcale.xcale.consulting.domain.Message;
import com.xcale.xcale.consulting.dto.MessageDTO;
import com.xcale.xcale.consulting.dto.MessageResponseDTO;
import com.xcale.xcale.consulting.exception.GroupBasicInformationException;
import com.xcale.xcale.consulting.exception.InexistentUserException;
import com.xcale.xcale.consulting.exception.MessageBasicInformationException;
import com.xcale.xcale.consulting.repository.ContactRepository;
import com.xcale.xcale.consulting.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class MessageService {

    private MessageRepository messageRepository;

    private ContactRepository contactRepository;

    private MessageResponseDTO messageDTO;

    @Autowired
    public MessageService(MessageRepository messageRepository, ContactRepository contactRepository) {
        this.messageRepository = messageRepository;
        this.contactRepository = contactRepository;
    }

    public MessageResponseDTO createMessage(MessageDTO message){
        messageDTO = new MessageResponseDTO();
        try{
            if( (message.getMsg() == null || message.getMsg().trim().isEmpty()) ||
                    message.getCreatedBy() == null ){
                throw new MessageBasicInformationException();
            }

            Optional<Contact> contact = contactRepository.findById(message.getCreatedBy().getId());
            if (!contact.isPresent())
                throw new InexistentUserException();

            Message msg = new Message();
            msg.setMsg(message.getMsg());
            msg.setCreatedBy(message.getCreatedBy());
            msg.setCreatedDate(new Date());

            messageRepository.save(msg);
            messageDTO.setMsg("Message created successfully.");
            messageDTO.setHttpStatus(HttpStatus.CREATED);

        }catch (MessageBasicInformationException e){
            messageDTO.setMsg("Verify the information provided.");
            messageDTO.setHttpStatus(HttpStatus.BAD_REQUEST);
        }catch (InexistentUserException e){
            messageDTO.setMsg("The message must have an existent user.");
            messageDTO.setHttpStatus(HttpStatus.BAD_REQUEST);
        }
        return messageDTO;
    }
}
