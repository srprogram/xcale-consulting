package com.xcale.xcale.consulting.dto;

import org.springframework.http.HttpStatus;

public class MessageResponseDTO {

    private Object msg;
    private HttpStatus httpStatus;

    public MessageResponseDTO(Object msg, HttpStatus httpStatus) {
        this.msg = msg;
        this.httpStatus = httpStatus;
    }

    public MessageResponseDTO() {
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
