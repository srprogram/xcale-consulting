package com.xcale.xcale.consulting.dto;

import com.xcale.xcale.consulting.domain.Contact;

import java.util.Date;

public class MessageDTO {

    private String msg;

    private Date createdDate;

    private Contact createdBy;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Contact getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Contact createdBy) {
        this.createdBy = createdBy;
    }
}
