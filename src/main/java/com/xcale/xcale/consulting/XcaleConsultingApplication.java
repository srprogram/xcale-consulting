package com.xcale.xcale.consulting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XcaleConsultingApplication {

	public static void main(String[] args) {
		SpringApplication.run(XcaleConsultingApplication.class, args);
	}

}
